<?php

namespace App;

use DB;
use App\Dummy;
use Illuminate\Database\Eloquent\Model;

class Dummy extends Model
{
    

	public function scopePass($query){

		return $query->where('score','>=',50);
	}

	static public function perc(){

		/* 
		* 
		* I want to load the percentile.so so that I can perform percentile function from laravel.
		* The percentile.so file is already placed in /home/developer/bin folder.
		*/
		 $conn = DB::select('select load_extension("/home/developer/bin/percentile")');
		 return $conn;

		$rec = DB::table('dummies')
				->where('score','>=',50)
				->sharedLock()
				->avg('score');
					

		return $rec;
	}

	public function scopeFail($query){
		return $query->where('score','<',50);
	}

/*
	public function scopeHighest($query){
		return $query->orderBy('score','desc')->get();
	}

	public function scopeLowest($query){
                return $query->orderBy('score','asc')->get();
        }
*/
}
