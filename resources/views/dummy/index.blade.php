@extends('layout')
@section('content')

<div class="row">

	<div class="col-md-6">
	<h2>Passed students</h2>
	@if(count($pass)>0)
	<span>95<sup>th</sup> Percentile score: {{ number_format($percentile,2) }} </span>

	<ul class="list-group">
	@foreach($pass as $student)
		<li class="list-group-item">{{ $student->name }} scored &nbsp; <span style="color:#0f0">{{ $student->score }}</span> &nbsp; in {{ $student->subject}}.</li>
	@endforeach
	</ul>

	@endif
	</div>

	<div class="col-md-6">
		<h2>Failed students</h2>
		@if(count($fail)>0)

	        <ul class="list-group">
	        @foreach($fail as $student)
	                <li class="list-group-item">{{ $student->name }} scored &nbsp; <span style="color:#f00">{{ $student->score }}</span> &nbsp; in {{ $student->subject}}.</li>
	        @endforeach
	        </ul>


		@endif
	</div>

</div>
@stop

